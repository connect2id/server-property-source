# File, S3 and DynamoDB - Based System Properties Source

Implements the 
`com.nimbusds.openid.connect.provider.spi.config.SystemPropertiesSource`
Service Provider Interface (SPI) from the Connect2id server SDK. Provides 
retrieval of Java system properties from a file, AWS S3 object or AWS DynamoDB 
item.


## Supported Connect2id server versions

* version 15.0+


## Usage

Set a Java system property `systemPropertiesURL` to a special URL specifying 
the properties' location.


### Local file

To retrieve properties located in a local file:

```
systemPropertiesURL = file:///[path]
```

Example:

```
file:////etc/c2id/server.properties
```

### AWS S3

To retrieve properties located in a AWS S3 object:

```
systemPropertiesURL = https://[s3-object-location]
```

Example:

```
systemPropertiesURL = https://s3-us-west-2.amazonaws.com/loh0uuji/ieg1koik
```

Example with an optional version ID for the object:

```
systemPropertiesURL = https://s3-us-west-2.amazonaws.com/loh0uuji/ieg1koik?version=e008d326-6ddc-4ef5-a57b-a08b14c16606
```

The AWS credentials for accessing the S3 bucket must be configured in way that 
the default AWS credentials provider chain can look them up, e.g. by setting 
the `aws.accessKeyId` and `aws.secretKey` Java system properties. See 
https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html


### AWS DynamoDB

To retrieve properties located in a AWS DynamoDB item:

```
systemPropertiesURL = dynamodb://[region]/[table]?
  hash_key_name=[key-name]
 &hash_key_value=[key-value]
 &range_key_name=[key-name]
 &range_key_value=[key-value]
 &attribute=[attribute-name]
```

The query parameters `range_key_name` and `range_key_value` are required only 
when a range key is used for the table.

Alternative format specifying a DynamoDB endpoint instead of region, works also 
with local DynamoDB instances:

```
systemPropertiesURL = http+dynamodb://[host-and-port]/[table]?
  hash_key_name=[key-name]
 &hash_key_value=[key-value]
 &range_key_name=[key-name]
 &range_key_value=[key-value]
 &attribute=[attribute-name]
``` 
 
Example:

```
dynamodb://us-west-2/config?hash_key_name=tid&hash_key_value=123&attribute=props
```

The AWS credentials for accessing the DynamoDB table must be configured in way 
that the default AWS credentials provider chain can look them up, e.g. by 
setting the `aws.accessKeyId` and `aws.secretKey` Java system properties. See 
http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html


## Maven

Maven coordinates:

```xml
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>c2id-server-property-source</artifactId>
    <version>[ version ]</version>
</dependency>
```


## Questions?

Contact [Connect2id support](http://connect2id.com/contact).


2024-01-29
