package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.StringInputStream;
import org.apache.commons.lang3.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Resource retriever for file, S3 and DynamoDB URLs.
 */
class ResourceRetriever {
	
	
	/**
	 * Retrieves the resource at the specified source URL.
	 *
	 * @param sourceURL The source URL.
	 *
	 * @return The input stream for the resource.
	 *
	 * @throws IOException If an I/O exception is encountered.
	 */
	static InputStream retrieve(final SourceURL sourceURL)
		throws IOException {
		
		if (sourceURL instanceof FileURL) {
			return retrieve((FileURL)sourceURL);
		}
		
		if (sourceURL instanceof S3URL) {
			return retrieve((S3URL)sourceURL);
		}
		
		if (sourceURL instanceof DynamoDBURL) {
			return retrieve((DynamoDBURL)sourceURL);
		}
		
		throw new IllegalArgumentException("Unexpected class: " + sourceURL.getClass());
	}
	
	
	/**
	 * Retrieves the resource at the specified source URL.
	 *
	 * @param fileURL The source URL.
	 *
	 * @return The input stream for the resource.
	 *
	 * @throws IOException If an I/O exception is encountered.
	 */
	static InputStream retrieve(final FileURL fileURL)
		throws IOException {
		
		return new FileInputStream(fileURL.getPath());
	}
	
	
	/**
	 * Retrieves the resource at the specified source URL.
	 *
	 * @param s3URL The source URL.
	 *
	 * @return The input stream for the resource.
	 *
	 * @throws IOException If an I/O exception is encountered.
	 */
	static InputStream retrieve(final S3URL s3URL)
		throws IOException {
		
		AmazonS3 client = AmazonS3ClientBuilder
			.standard()
			.withCredentials(new DefaultAWSCredentialsProviderChain())
			.withRegion(s3URL.getRegion())
			.build();
		try {
			 S3Object o = client.getObject(
				new GetObjectRequest(
					s3URL.getBucketName(),
					s3URL.getKeyName(),
					s3URL.getVersionID()));
			
			if (o == null) {
				throw new IOException("Not found");
			}
			
			return o.getObjectContent();
			
		} catch (Exception e) {
			throw new IOException("Couldn't retrieve resource at S3 URL: " + s3URL.getURI() + ": " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Retrieves the resource at the specified source URL.
	 *
	 * @param dynamoDBURL The source URL.
	 *
	 * @return The input stream for the resource.
	 *
	 * @throws IOException If an I/O exception is encountered.
	 */
	static InputStream retrieve(final DynamoDBURL dynamoDBURL)
		throws IOException {
		
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(new DefaultAWSCredentialsProviderChain())
			.withRegion(dynamoDBURL.getRegion())
			.build();
		
		try {
			DynamoDB dynamoDB = new DynamoDB(client);
			
			Table table = dynamoDB.getTable(dynamoDBURL.getTableName());
			
			if (table == null) {
				throw new IOException("No such table: " + dynamoDBURL.getTableName());
			}
			
			Item item = table.getItem(new GetItemSpec().withPrimaryKey(dynamoDBURL.getPrimaryKey()));
			
			if (item == null) {
				throw new IOException("No such item: " + dynamoDBURL.getPrimaryKey());
			}
			
			String content = item.getString(dynamoDBURL.getAttributeName());
			
			if (StringUtils.isBlank(content)) {
				throw new IOException("No such attribute: " + dynamoDBURL.getAttributeName());
			}
			
			return new StringInputStream(content);
			
		} catch (Exception e) {
			throw new IOException("Couldn't retrieve resource at DynamoDB URL: " + dynamoDBURL.getURI() + ": " + e.getMessage(), e);
		}
	}
}
