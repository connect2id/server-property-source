/**
 * SPI implementation of
 * {@link com.nimbusds.openid.connect.provider.spi.config.SystemPropertiesSource}
 * for retrieving system properties from file, AWS S3 and AWS DynamoDB URLs.
 */
package com.nimbusds.openid.connect.provider.spi.config.source;