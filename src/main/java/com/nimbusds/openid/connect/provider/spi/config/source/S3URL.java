package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.regions.Regions;
import com.nimbusds.common.util.URLUtility;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;


/**
 * AWS S3 URL.
 *
 * <p>Example:
 *
 * <pre>
 * https://s3-us-west-2.amazonaws.com/loh0uuji/ieg1koik
 * </pre>
 *
 * <p>See https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingBucket.html#access-bucket-intro
 */
public class S3URL extends SourceURL {
	
	
	/**
	 * The AWS region.
	 */
	private final Regions region;
	
	
	/**
	 * The S3 bucket name.
	 */
	private final String bucketName;
	
	
	/**
	 * The resource key name inside the S3 bucket.
	 */
	private final String keyName;
	
	
	/**
	 * Optional version ID for the resource.
	 */
	private final String versionID;
	
	
	/**
	 * Creates a new AWS S3 URL from the specified URI.
	 *
	 * @param uri The URI.
	 */
	public S3URL(final URI uri) {
		
		super(uri);
		
		if (! "http".equalsIgnoreCase(uri.getScheme()) && ! "https".equalsIgnoreCase(uri.getScheme())) {
			throw new IllegalArgumentException("Not a S3 http(s) scheme");
		}
		
		if (uri.getAuthority() == null) {
			throw new IllegalArgumentException("Missing authority");
		}
		
		if (! uri.getAuthority().toLowerCase().endsWith("amazonaws.com")) {
			throw new IllegalArgumentException("Not a S3 bucketName URL");
		}
		
		int numDotsInAuthority = StringUtils.countMatches(uri.getAuthority(), '.');
		
		if (3 == numDotsInAuthority || 4 == numDotsInAuthority) {
			
			// Virtual host format:
			// 1. https://bucket.s3.amazonaws.com/key-name
			// 2. https://bucket.s3-[aws-region].amazonaws.com/key-name
			// 3. https://bucket.s3.[aws-region].amazonaws.com/key-name
			
			String[] authorityParts = uri.getAuthority().split("\\.");
			
			bucketName = authorityParts[0];
			
			if (uri.getAuthority().equalsIgnoreCase(bucketName + ".s3.amazonaws.com") && 3 == numDotsInAuthority) {
				// 1. https://bucket.s3.amazonaws.com/key-name
				region = null;
			} else if (authorityParts[1].toLowerCase().startsWith("s3-") && 3 == numDotsInAuthority) {
				// 2. https://bucket.s3-[aws-region].amazonaws.com/key-name
				region = Regions.fromName(authorityParts[1].substring("s3-".length()));
			} else if (authorityParts[1].equalsIgnoreCase("s3") && 4 == numDotsInAuthority) {
				// 3. https://bucket.s3.[aws-region].amazonaws.com/key-name
				region = Regions.fromName(authorityParts[2]);
			} else {
				throw new IllegalArgumentException("Not a S3 bucketName URL");
			}
			
			if (StringUtils.isBlank(uri.getPath()) || uri.getPath().equalsIgnoreCase("/")) {
				throw new IllegalArgumentException("Missing resource key in URL path");
			}
			
			keyName = uri.getPath().substring(1); // remove leading '/' from path
			
			if (StringUtils.isBlank(keyName)) {
				throw new IllegalArgumentException("Missing resource key in URL path");
			}
			
		} else if (2 == numDotsInAuthority) {
			
			// Path format:
			// US East (N. Virginia) Region endpoint: https://s3.amazonaws.com/bucket/key-name
			// Region-specific endpoint: https://s3-aws-region.amazonaws.com/bucket/key-name
			
			String[] authorityParts = uri.getAuthority().split("\\.");
			
			if (uri.getAuthority().equalsIgnoreCase("s3.amazonaws.com")) {
				region = Regions.US_EAST_1;
			} else if (uri.getAuthority().toLowerCase().startsWith("s3-") && uri.getAuthority().toLowerCase().endsWith(".amazonaws.com")) {
				region = Regions.fromName(authorityParts[0].substring("s3-".length()));
			} else {
				throw new IllegalArgumentException("Couldn't determine AWS region from URL");
			}
			
			if (StringUtils.isBlank(uri.getPath()) || uri.getPath().equalsIgnoreCase("/")) {
				throw new IllegalArgumentException("Missing S3 bucket name in URL path");
			}
			
			// Strip leading '/', then split into bucket, remainder is key name
			String[] pathParts = uri.getPath().substring(1).split("/", 2);
			
			if (pathParts.length < 1) {
				throw new IllegalArgumentException("Missing S3 bucket name in URL path");
			}
			
			bucketName = pathParts[0];
			
			if (pathParts.length < 2) {
				throw new IllegalArgumentException("Missing resource key in URL path");
			}
			
			keyName = pathParts[1];
			
			if (StringUtils.isBlank(keyName)) {
				throw new IllegalArgumentException("Missing resource key in URL path");
			}
			
		} else {
			throw new IllegalArgumentException("Not a S3 bucketName URL");
		}
		
		if (StringUtils.isNotBlank(uri.getQuery())) {
			versionID = URLUtility.parseParameters(uri.getQuery()).get("version");
		} else {
			versionID = null;
		}
	}
	
	
	/**
	 * Returns the AWS region.
	 *
	 * @return The region.
	 */
	public Regions getRegion() {
		return region;
	}
	
	
	/**
	 * Returns the S3 bucket name.
	 *
	 * @return The bucket name.
	 */
	public String getBucketName() {
		return bucketName;
	}
	
	
	/**
	 * Returns the resource key name.
	 *
	 * @return The key name.
	 */
	public String getKeyName() {
		return keyName;
	}
	
	
	/**
	 * Returns the optional version ID for the resource.
	 *
	 * @return The version ID, {@code null} if not specified.
	 */
	public String getVersionID() {
		return versionID;
	}
}
