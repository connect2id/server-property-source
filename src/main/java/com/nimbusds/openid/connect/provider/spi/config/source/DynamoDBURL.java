package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.nimbusds.common.util.URLUtility;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.Map;


/**
 * AWS DynamoDB URL.
 *
 * <p>Example:
 *
 * <pre>
 * dynamodb://us-west-2/config?hash_key_name=tid&amp;hash_key_value=123&amp;attribute=props
 * </pre>
 */
public class DynamoDBURL extends SourceURL {
	
	
	/**
	 * The AWS region, {@code null} if the endpoint is set instead.
	 */
	private final Regions region;
	
	
	/**
	 * The DynamoDB endpoint, {@code null} if the region is set instead.
	 */
	private final String endpoint;
	
	
	/**
	 * The DynamoDB table.
	 */
	private final String tableName;
	
	
	/**
	 * The primary key for retrieving the resource.
	 */
	private final PrimaryKey primaryKey;
	
	
	/**
	 * The item attribute name containing the resource.
	 */
	private final String attributeName;
	
	
	/**
	 * Creates a new DynamoDB URL from the specified URI.
	 *
	 * @param uri The URI.
	 */
	public DynamoDBURL(URI uri) {
		super(uri);
		
		if (StringUtils.isBlank(uri.getAuthority())) {
			throw new IllegalArgumentException("Missing AWS region or endpoint");
		}
		
		if ("dynamodb".equalsIgnoreCase(uri.getScheme())) {
			region = Regions.fromName(uri.getAuthority());
			endpoint = null;
		} else if ("http+dynamodb".equalsIgnoreCase(uri.getScheme())) {
			endpoint = uri.getAuthority();
			region = null;
		} else {
			throw new IllegalArgumentException("Not a dynamodb or http+dynamodb scheme");
		}
		
		if (StringUtils.isBlank(uri.getPath()) || uri.getPath().equalsIgnoreCase("/")) {
			throw new IllegalArgumentException("Missing DynamoDB table name");
		}
		
		tableName = uri.getPath().substring(1); // strip leading '/'
		
		if (StringUtils.isBlank(uri.getQuery())) {
			throw new IllegalArgumentException("Missing query spec");
		}
		
		Map<String,String> queryParams = URLUtility.parseParameters(uri.getQuery());
		
		String hashKeyName = queryParams.get("hash_key_name");
		
		if (StringUtils.isBlank(hashKeyName)) {
			throw new IllegalArgumentException("Missing key_name query parameter");
		}
		
		String hashKeyValue = queryParams.get("hash_key_value");
		
		if (StringUtils.isBlank(hashKeyValue)) {
			throw new IllegalArgumentException("Missing key_value query parameter");
		}
		
		String rangeKeyName = queryParams.get("range_key_name");
		String rangeKeyValue = queryParams.get("range_key_value");
		
		if (StringUtils.isNotBlank(rangeKeyName) && StringUtils.isNotBlank(rangeKeyValue)) {
			primaryKey = new PrimaryKey(hashKeyName, hashKeyValue, rangeKeyName, rangeKeyValue);
		} else {
			primaryKey = new PrimaryKey(hashKeyName, hashKeyValue);
		}
		
		attributeName = queryParams.get("attribute");
		
		if (StringUtils.isBlank(attributeName)) {
			throw new IllegalArgumentException("Missing attribute query parameter");
		}
	}
	
	
	/**
	 * Returns the AWS region.
	 *
	 * @return The AWS region, {@code null} if an {@link #getEndpoint()} is
	 *         specified instead.
	 */
	public Regions getRegion() {
		return region;
	}
	
	
	/**
	 * Returns the DynamoDB endpoint.
	 *
	 * @return The DynamoDB endpoint, {@code null} if a
	 *         {@link #getRegion()} is specified instead.
	 */
	public String getEndpoint() {
		return endpoint;
	}
	
	
	/**
	 * Returns the DynamoDB table name.
	 *
	 * @return The table name.
	 */
	public String getTableName() {
		return tableName;
	}
	
	
	/**
	 * Returns the primary key consisting of hash key and optional range
	 * key.
	 *
	 * @return The primary key.
	 */
	public PrimaryKey getPrimaryKey() {
		return primaryKey;
	}
	
	
	/**
	 * Returns the name of the attribute containing the resource.
	 *
	 * @return The attribute name.
	 */
	public String getAttributeName() {
		return attributeName;
	}
}
