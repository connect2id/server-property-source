package com.nimbusds.openid.connect.provider.spi.config.source;


import org.apache.commons.lang3.StringUtils;

import java.net.URI;


/**
 * File URL.
 *
 * <p>Example:
 *
 * <pre>
 * file:////etc/c2id/server.properties
 * </pre>
 */
public class FileURL extends SourceURL {
	
	
	/**
	 * Creates a new file URL from the specified URI.
	 *
	 * @param uri The URI.
	 */
	public FileURL(final URI uri) {
		super(uri);
		
		if (! "file".equalsIgnoreCase(uri.getScheme())) {
			throw new IllegalArgumentException("Not a file scheme");
		}
		
		if (StringUtils.isBlank(uri.getPath()) || uri.getPath().equalsIgnoreCase("/")) {
			throw new IllegalArgumentException("Missing file path");
		}
	}
	
	
	/**
	 * Returns the file path.
	 *
	 * @return The file path.
	 */
	public String getPath() {
		return getURI().getPath();
	}
}
