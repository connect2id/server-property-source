package com.nimbusds.openid.connect.provider.spi.config.source;


import java.net.URI;


/**
 * The base class for source URLs.
 */
public abstract class SourceURL {
	
	
	/**
	 * The underlying URI.
	 */
	private final URI uri;
	
	
	/**
	 * Creates a new source URL.
	 *
	 * @param uri The underlying URI. Must not be {@code null}/
	 */
	public SourceURL(URI uri) {
		if (uri == null) {
			throw new IllegalArgumentException("The URI must not be null");
		}
		this.uri = uri;
	}
	
	
	/**
	 * Returns the underlying URI.
	 *
	 * @return The URI.
	 */
	public URI getURI() {
		return uri;
	}
	
	
	@Override
	public String toString() {
		return uri.toString();
	}
}
