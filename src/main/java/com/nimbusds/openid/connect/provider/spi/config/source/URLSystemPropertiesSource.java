package com.nimbusds.openid.connect.provider.spi.config.source;


import com.nimbusds.openid.connect.provider.spi.config.SystemPropertiesSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kohsuke.MetaInfServices;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


/**
 * System properties source, supports retrieval from file, AWS S3 and AWS
 * DynamoDB URLs.
 *
 * <p>This class is thread-safe.
 */
@MetaInfServices(SystemPropertiesSource.class)
public class URLSystemPropertiesSource implements SystemPropertiesSource {
	
	
	/**
	 * The name of the system property specifying the system properties
	 * URL.
	 */
	public static final String SYSTEM_PROPERTY_NAME = "systemPropertiesURL";
	
	
	/**
	 * Gets the properties at the specified source URL.
	 *
	 * @param sourceURL The source URL.
	 *
	 * @return The properties.
	 *
	 * @throws RuntimeException If retrieval of the properties failed.
	 */
	public Properties getProperties(final SourceURL sourceURL) {
		
		Properties properties = new Properties();
		
		try {
			properties.load(ResourceRetriever.retrieve(sourceURL));
		} catch (IOException e) {
			throw new RuntimeException("Couldn't retrieve system properties: " + e.getMessage(), e);
		}
		
		LogManager.getLogger("MAIN").info("[SP0003] URLSystemPropertiesSource: Retrieved " + properties.size() + " properties from " + sourceURL + ": " + properties.keySet());
		
		return properties;
	}
	
	
	/**
	 * {@inheritDoc}
	 *
	 * @throws RuntimeException If retrieval of the properties failed.
	 */
	@Override
	public Properties getProperties() {
		
		Logger log = LogManager.getLogger("MAIN");
		
		String sysPropVal = System.getProperty(SYSTEM_PROPERTY_NAME);
		
		if (StringUtils.isBlank(sysPropVal)) {
			// URL not specified
			log.info("[SP0001] URLSystemPropertiesSource: No properties retrieved, " + SYSTEM_PROPERTY_NAME + " not set");
			return new Properties();
		}
		
		URI uri;
		
		try {
			uri = new URI(sysPropVal);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Invalid " + SYSTEM_PROPERTY_NAME + ": " + sysPropVal);
		}
		
		log.info("[SP0002] URLSystemPropertiesSource: Found " + SYSTEM_PROPERTY_NAME + ": " + uri);
		
		SourceURL sourceURL;
		try {
			sourceURL = new FileURL(uri);
		} catch (IllegalArgumentException e) {
			// try next format
			try {
				sourceURL = new S3URL(uri);
			} catch (IllegalArgumentException e2) {
				// try next format
				try {
					sourceURL = new DynamoDBURL(uri);
				} catch (IllegalArgumentException e3) {
					// give up
					throw new RuntimeException("Invalid " + SYSTEM_PROPERTY_NAME + ": " + uri);
				}
			}
		}
		
		return getProperties(sourceURL);
	}
}
