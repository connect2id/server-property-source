package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import junit.framework.TestCase;

import java.net.URI;
import java.util.Arrays;


public class DynamoDBURLTest extends TestCase {
	
	
	public void testCreateWithRegionSpec() {
		
		URI uri = URI.create("dynamodb://eu-central-1/config?hash_key_name=tid&hash_key_value=123&attribute=properties");
		
		var dynamoDBURL = new DynamoDBURL(uri);
		
		assertEquals(uri, dynamoDBURL.getURI());
		
		assertEquals(Regions.EU_CENTRAL_1, dynamoDBURL.getRegion());
		assertNull(dynamoDBURL.getEndpoint());
		assertEquals("config", dynamoDBURL.getTableName());
		assertEquals("tid", dynamoDBURL.getPrimaryKey().getComponents().iterator().next().getName());
		assertEquals("123", dynamoDBURL.getPrimaryKey().getComponents().iterator().next().getValue());
		assertEquals(1, dynamoDBURL.getPrimaryKey().getComponents().size());
		assertEquals("properties", dynamoDBURL.getAttributeName());
	}
	
	
	public void testCreateWithEndpointSpec() {
		
		URI uri = URI.create("http+dynamodb://localhost:8080/config?hash_key_name=tid&hash_key_value=123&attribute=properties");
		
		var dynamoDBURL = new DynamoDBURL(uri);
		
		assertEquals(uri, dynamoDBURL.getURI());
		
		assertNull(dynamoDBURL.getRegion());
		assertEquals("localhost:8080", dynamoDBURL.getEndpoint());
		assertEquals("config", dynamoDBURL.getTableName());
		assertEquals("tid", dynamoDBURL.getPrimaryKey().getComponents().iterator().next().getName());
		assertEquals("123", dynamoDBURL.getPrimaryKey().getComponents().iterator().next().getValue());
		assertEquals(1, dynamoDBURL.getPrimaryKey().getComponents().size());
		assertEquals("properties", dynamoDBURL.getAttributeName());
	}
	
	
	public void testCreateWithRegionSpecAndOptionalRangeKey() {
		
		URI uri = URI.create("dynamodb://eu-central-1/config?hash_key_name=tid&hash_key_value=123&range_key_name=group&range_key_value=456&attribute=properties");
		
		var dynamoDBURL = new DynamoDBURL(uri);
		
		assertEquals(uri, dynamoDBURL.getURI());
		
		assertEquals(Regions.EU_CENTRAL_1, dynamoDBURL.getRegion());
		assertNull(dynamoDBURL.getEndpoint());
		assertEquals("config", dynamoDBURL.getTableName());
		
		for (KeyAttribute ka: dynamoDBURL.getPrimaryKey().getComponents()) {
			
			assertTrue(Arrays.asList("tid", "group").contains(ka.getName()));
			
			if (ka.getName().equals("tid")) {
				assertEquals("123", ka.getValue());
			} else if (ka.getName().equals("group")) {
				assertEquals("456", ka.getValue());
			} else {
				fail();
			}
		}
		assertEquals(2, dynamoDBURL.getPrimaryKey().getComponents().size());
		
		assertEquals("properties", dynamoDBURL.getAttributeName());
	}
	
	
	public void testInvalidScheme() {
		
		try {
			URI uri = URI.create("http://eu-central-1/config?hash_key_name=tid&hash_key_value=123&range_key_name=group&range_key_value=456&attribute=properties");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Not a dynamodb or http+dynamodb scheme", e.getMessage());
		}
	}
	
	
	public void testMissingAuthority() {
		
		try {
			URI uri = URI.create("dynamodb:///config?hash_key_name=tid&hash_key_value=123&range_key_name=group&range_key_value=456&attribute=properties");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing AWS region or endpoint", e.getMessage());
		}
	}
	
	
	public void testMissingTableName() {
		
		try {
			URI uri = URI.create("dynamodb://eu-central-1/?hash_key_name=tid&hash_key_value=123&range_key_name=group&range_key_value=456&attribute=properties");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing DynamoDB table name", e.getMessage());
		}
	}
	
	
	public void testMissingQuery() {
		
		try {
			URI uri = URI.create("dynamodb://eu-central-1/config");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing query spec", e.getMessage());
		}
	}
	
	
	public void testMissingHashKeyName() {
		
		try {
			URI uri = URI.create("dynamodb://eu-central-1/config?hash_key_value=123&range_key_name=group&range_key_value=456&attribute=properties");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing key_name query parameter", e.getMessage());
		}
	}
	
	
	public void testMissingHashKeyValue() {
		
		try {
			URI uri = URI.create("dynamodb://eu-central-1/config?hash_key_name=tid&range_key_name=group&range_key_value=456&attribute=properties");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing key_value query parameter", e.getMessage());
		}
	}
	
	
	public void testMissingAttributeName() {
		
		try {
			URI uri = URI.create("dynamodb://eu-central-1/config?hash_key_name=tid&hash_key_value=123&range_key_name=group&range_key_value=456");
			new DynamoDBURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing attribute query parameter", e.getMessage());
		}
	}
}
