package com.nimbusds.openid.connect.provider.spi.config.source;


import junit.framework.TestCase;

import java.net.URI;


public class FileURLTest extends TestCase {
	
	
	public void testCreate() {
		
		URI uri = URI.create("file:///etc/c2id/server.properties");
		
		var fileURL = new FileURL(uri);
		
		assertEquals(uri, fileURL.getURI());
		assertEquals("/etc/c2id/server.properties", fileURL.getPath());
	}
	
	
	public void testCreateWithAuthority() {
		
		URI uri = URI.create("file://localhost/etc/c2id/server.properties");
		
		var fileURL = new FileURL(uri);
		
		assertEquals(uri, fileURL.getURI());
		assertEquals("/etc/c2id/server.properties", fileURL.getPath());
	}
	
	
	public void testInvalidScheme() {
		
		URI uri = URI.create("http://s3-eu-central-1.amazonaws.com/url-system-properties-source-test-nicknoyd/config/123");
		
		try {
			new FileURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Not a file scheme", e.getMessage());
		}
	}
	
	
	public void testMissingPath() {
		
		URI uri = URI.create("file:///");
		
		try {
			new FileURL(uri);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing file path", e.getMessage());
		}
	}
}
