package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.regions.Regions;
import junit.framework.TestCase;

import java.net.URI;


public class S3URLTest extends TestCase {
	
	
	public void testCreateVirtualFormatNoRegion() {
		
		// http://bucket.s3.amazonaws.com/key-name
		
		URI uri = URI.create("https://loh0uuji.s3.amazonaws.com/ieg1koik");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertNull(s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik", s3URL.getKeyName());
	}
	
	
	public void testCreateVirtualFormatWithRegion() {
		
		// http://bucket.s3-aws-region.amazonaws.com/key-name
		
		URI uri = URI.create("https://loh0uuji.s3-us-west-2.amazonaws.com/ieg1koik");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.US_WEST_2, s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik", s3URL.getKeyName());
	}
	
	
	public void testCreateNewVirtualFormatWithRegion() {
		
		// http://bucket.s3.aws-region.amazonaws.com/key-name
		
		URI uri = URI.create("https://loh0uuji.s3.us-west-2.amazonaws.com/ieg1koik");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.US_WEST_2, s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik", s3URL.getKeyName());
	}
	
	
	public void testCreateVirtualFormatWithRegionAndPath() {
		
		// http://bucket.s3-aws-region.amazonaws.com/key-name
		
		URI uri = URI.create("https://loh0uuji.s3-us-west-2.amazonaws.com/ieg1koik/c2id.properties");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.US_WEST_2, s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik/c2id.properties", s3URL.getKeyName());
	}
	
	
	public void testCreateNewVirtualFormatWithRegionAndPath() {
		
		// http://bucket.s3-aws-region.amazonaws.com/key-name
		
		URI uri = URI.create("https://c2id-test.s3.eu-west-2.amazonaws.com/sample-config/c2id.properties");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.EU_WEST_2, s3URL.getRegion());
		assertEquals("c2id-test", s3URL.getBucketName());
		assertEquals("sample-config/c2id.properties", s3URL.getKeyName());
	}
	
	
	public void testCreatePathFormatDefaultRegion() {
		
		// US East (N. Virginia) Region endpoint: http://s3.amazonaws.com/bucket/key-name
		
		URI uri = URI.create("https://s3.amazonaws.com/loh0uuji/ieg1koik");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.US_EAST_1, s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik", s3URL.getKeyName());
	}
	
	
	public void testCreatePathFormatOtherRegion() {
		
		// US East (N. Virginia) Region endpoint: http://s3.amazonaws.com/bucket/key-name
		
		URI uri = URI.create("https://s3-us-west-2.amazonaws.com/loh0uuji/ieg1koik");
		
		var s3URL = new S3URL(uri);
		
		assertEquals(uri, s3URL.getURI());
		
		assertEquals(Regions.US_WEST_2, s3URL.getRegion());
		assertEquals("loh0uuji", s3URL.getBucketName());
		assertEquals("ieg1koik", s3URL.getKeyName());
	}
	
	
	public void testParseVirtualFormatInvalidScheme() {
		
		try {
			new S3URL(URI.create("ftp://loh0uuji.s3-us-west-2.amazonaws.com/ieg1koik"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Not a S3 http(s) scheme", e.getMessage());
		}
	}
	
	
	public void testParseVirtualFormatMissingAuthority() {
		
		try {
			new S3URL(URI.create("https:///loh0uuji"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing authority", e.getMessage());
		}
	}
	
	
	public void testParseVirtualFormatMissingPath() {
		
		try {
			new S3URL(URI.create("https://loh0uuji.s3-us-west-2.amazonaws.com"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing resource key in URL path", e.getMessage());
		}
	}
	
	
	public void testParseVirtualFormatMissingKeyName() {
		
		try {
			new S3URL(URI.create("https://loh0uuji.s3-us-west-2.amazonaws.com/"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing resource key in URL path", e.getMessage());
		}
	}
	
	
	public void testParseVirtualFormatMissingKeyNameAlt() {
		
		try {
			new S3URL(URI.create("https://loh0uuji.s3-us-west-2.amazonaws.com"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing resource key in URL path", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatInvalidScheme() {
		
		try {
			new S3URL(URI.create("ftp://s3-us-west-2.amazonaws.com/loh0uuji/ieg1koik"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Not a S3 http(s) scheme", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatMissingAuthority() {
		
		try {
			new S3URL(URI.create("https:///loh0uuji/ieg1koik"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing authority", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatMissingPath() {
		
		try {
			new S3URL(URI.create("https://s3-us-west-2.amazonaws.com"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing S3 bucket name in URL path", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatMissingBucketAlt() {
		
		try {
			new S3URL(URI.create("https://s3-us-west-2.amazonaws.com/"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing S3 bucket name in URL path", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatMissingKeyName() {
		
		try {
			new S3URL(URI.create("https://s3-us-west-2.amazonaws.com/loh0uuji/"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing resource key in URL path", e.getMessage());
		}
	}
	
	
	public void testParsePathFormatMissingKeyNameAlt() {
		
		try {
			new S3URL(URI.create("https://s3-us-west-2.amazonaws.com/loh0uuji"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Missing resource key in URL path", e.getMessage());
		}
	}
}
