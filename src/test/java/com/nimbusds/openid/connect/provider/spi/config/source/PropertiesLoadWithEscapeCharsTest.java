package com.nimbusds.openid.connect.provider.spi.config.source;


import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


public class PropertiesLoadWithEscapeCharsTest extends TestCase {
	
	
	public void testEscapedColon()
		throws Exception {
		
		var in = new Properties();
		in.setProperty("url", "https://c2id.com");
		
		var bos = new ByteArrayOutputStream();
		
		in.store(bos, "");
		
		String content = bos.toString(StandardCharsets.UTF_8);
		
		System.out.println(content);
		
		String[] lines = content.split("\n");
		assertEquals("#", lines[0]);
		assertTrue(lines[1].startsWith("#")); // second line is date of write
		assertEquals("url=https\\://c2id.com", lines[2]);
		assertEquals(3, lines.length);
		
		var out = new Properties();
		out.load(new ByteArrayInputStream(bos.toByteArray()));
		
		assertEquals("https://c2id.com", out.getProperty("url"));
	}
}
