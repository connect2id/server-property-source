package com.nimbusds.openid.connect.provider.spi.config.source;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import junit.framework.TestCase;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Properties;


/**
 * The test requires AWS credentials for S3 and DynamoDB in the
 * {@code eu-central-1} region.
 */
public class URLSystemPropertiesSourceTest extends TestCase {
	
	
	@After
	@Override
	public void tearDown() {
		
		System.getProperties().remove(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME);
	}
	
	
	public void testConstant() {
		
		assertEquals("systemPropertiesURL", URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME);
	}
	
	
	static void validateRetrievedProperties(final Properties properties) {
		
		assertEquals("https://openid.c2id.com", properties.getProperty("op.issuer"));
		assertEquals("https://openid.c2id.com/login", properties.getProperty("op.authz.endpoint"));
		assertEquals(2, properties.size());
	}
	
	
	public void testRetrieveFromFile() {
		
		String currentDir = System.getProperty("user.dir");
		
		URI fileURI = URI.create("file:///" + currentDir + "/sampleFile.properties");
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, fileURI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		Properties properties = source.getProperties();
		
		validateRetrievedProperties(properties);
	}
	
	
	public void testRetrieveFromNonExistingFile() {
		
		String currentDir = System.getProperty("user.dir");
		
		URI fileURI = URI.create("file:///" + currentDir + "/noSuchFile.properties");
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, fileURI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		try {
			source.getProperties();
			fail();
		} catch (RuntimeException e) {
			assertTrue(e.getMessage().startsWith("Couldn't retrieve system properties: "));
		}
	}
	
	
	public void testRetrieveFromS3URL()
		throws IOException {
		
		AmazonS3 client = AmazonS3ClientBuilder
			.standard()
			.withCredentials(new DefaultAWSCredentialsProviderChain())
			.withRegion(Regions.EU_CENTRAL_1)
			.build();
		
		String bucketName = "url-system-properties-source-test-" + RandomStringUtils.randomAlphabetic(8).toLowerCase();
		
		System.out.println("Bucket name: " + bucketName);
		
		client.createBucket(bucketName);
		
		String key = "config/123";
		
		client.putObject(new PutObjectRequest(bucketName, key, new FileInputStream("sampleFile.properties"), new ObjectMetadata()));
		
		URI s3URI = URI.create("https://s3-eu-central-1.amazonaws.com/" + bucketName + "/" + key);
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, s3URI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		Properties properties = source.getProperties();
		
		validateRetrievedProperties(properties);
		
		ObjectListing objectListing = client.listObjects(bucketName);
		
		for (S3ObjectSummary s3ObjectSummary: objectListing.getObjectSummaries()) {
			client.deleteObject(new DeleteObjectRequest(bucketName, s3ObjectSummary.getKey()));
		}
		
		client.deleteBucket(bucketName);
	}
	
	
	public void testRetrieveFromInvalidS3URL() {
		
		String bucketName = "url-system-properties-source-test-" + RandomStringUtils.randomAlphabetic(8).toLowerCase();
		
		String key = "config/123";
		
		URI s3URI = URI.create("https://s3-eu-central-1.amazonaws.com/" + bucketName + "/" + key);
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, s3URI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		try {
			source.getProperties();
			fail();
		} catch (RuntimeException e) {
			assertTrue(e.getMessage().startsWith(
				"Couldn't retrieve system properties: " +
					"Couldn't retrieve resource at S3 URL: https://s3-eu-central-1.amazonaws.com/"+bucketName+"/config/123: The specified bucket does not exist"
			));
		}
	}
	
	
	public void testRetrieveFromDynamoDBURL()
		throws IOException, InterruptedException {
		
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(new DefaultAWSCredentialsProviderChain())
			.withRegion(Regions.EU_CENTRAL_1)
			.build();
		
		String tableName = "url-system-properties-source-test-" + RandomStringUtils.randomAlphabetic(8).toLowerCase();
		
		System.out.println("Table name: " + tableName);
		
		DynamoDB dynamoDB = new DynamoDB(client);
		
		Table table = dynamoDB.createTable(new CreateTableRequest(
			tableName,
			Collections.singletonList(new KeySchemaElement("tid", KeyType.HASH)))
			.withAttributeDefinitions(Collections.singletonList(new AttributeDefinition("tid", ScalarAttributeType.S)))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L))
		);
		
		table.waitForActive();
		
		String content = new String(Files.readAllBytes(Paths.get("sampleFile.properties")), StandardCharsets.UTF_8);
		
		table.putItem(
			new Item()
				.withPrimaryKey(new PrimaryKey("tid", "123"))
				.with("properties", content)
		);
		
		URI dynamoDBURI = URI.create("dynamodb://eu-central-1/"+tableName+"?hash_key_name=tid&hash_key_value=123&attribute=properties");
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, dynamoDBURI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		Properties properties = source.getProperties();
		
		validateRetrievedProperties(properties);
		
		client.deleteTable(tableName);
	}
	
	
	public void testInvalidDynamoDBURL() {
		
		String tableName = "url-system-properties-source-test-" + RandomStringUtils.randomAlphabetic(8).toLowerCase();
		
		System.out.println("Table name: " + tableName);URI dynamoDBURI = URI.create("dynamodb://eu-central-1/"+tableName+"?hash_key_name=tid&hash_key_value=123&attribute=properties");
		
		System.setProperty(URLSystemPropertiesSource.SYSTEM_PROPERTY_NAME, dynamoDBURI.toString());
		
		var source = new URLSystemPropertiesSource();
		
		try {
			source.getProperties();
			fail();
		} catch (RuntimeException e) {
			assertTrue(e.getMessage().startsWith(
				"Couldn't retrieve system properties: " +
					"Couldn't retrieve resource at DynamoDB URL: dynamodb://eu-central-1/"+tableName+"?hash_key_name=tid&hash_key_value=123&attribute=properties: Requested resource not found"
			));
		}
	}
}
